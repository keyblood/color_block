﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
public class GameLoseScript : MonoBehaviour {
	public Text TCoin;
	public Text TScore;
	public Text TLevel;


	void Start(){
		TCoin.text = PlayerPrefs.GetInt ("Coin").ToString();
		TScore.text = PlayerPrefs.GetInt ("Score").ToString();
		TLevel.text = PlayerPrefs.GetInt ("Level").ToString();
	}
	public void RetryGame(){
		Application.LoadLevel ("ScenePlay");
	}


	public void MainMenuGame(){
		Application.LoadLevel ("SceneMain");
	}


	public void ShareGame(){
		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
	}

	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			// Signal an app activation App Event
			FB.ActivateApp();
			var perms = new List<string>(){"public_profile", "email", "user_friends"};
			//FB.LogInWithReadPermissions(perms, AuthCallback);
			FB.ShareLink(
				new Uri("https://developers.facebook.com/"),
				callback: ShareCallback
			);
		} else {
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}


	private void AuthCallback (ILoginResult result) {
		if (FB.IsLoggedIn) {
			// AccessToken class will have session details
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			Debug.Log(aToken.UserId);
			// Print current access token's granted permissions
			foreach (string perm in aToken.Permissions) {
				Debug.Log(perm);
			}
		} else {
			Debug.Log("User cancelled login");
		}
	}


	private void ShareCallback (IShareResult result) {
	    if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
	        Debug.Log("ShareLink Error: "+result.Error);
	    } else if (!String.IsNullOrEmpty(result.PostId)) {
	        // Print post identifier of the shared content
	        Debug.Log(result.PostId);
	    } else {
	        // Share succeeded without postID
	        Debug.Log("ShareLink success!");
	    }
	}


}
