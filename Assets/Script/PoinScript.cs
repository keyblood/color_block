﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoinScript : MonoBehaviour {
	public GameObject Bullet;
	public float huong;
	public float time=0;
	public float timeDelay=3f;
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if(this.transform.childCount<1 && time>timeDelay){
			Vector3 vt3 = new Vector3 (this.transform.position.x,this.transform.position.y+Random.Range(0,0.4f),this.transform.position.z);
			GameObject gob = Instantiate (Bullet, vt3, Quaternion.identity) as GameObject;
			gob.transform.SetParent (this.transform);
			time = 0;
		}
	}
	void OnTriggerEnter(Collider other){
		if (other.tag == "Dan") {
			if (other.GetComponent<BulletScript> ().checkMove == true) {
				Destroy (other.gameObject);
			}
		}
	}
}
