﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	public float speed;
	public PoinScript parent;
	public bool checkMove;
	void Start(){
		parent = this.transform.parent.GetComponent<PoinScript> ();
	}
	void Update(){
		transform.Translate (Vector3.right * parent.huong * speed * Time.deltaTime);
	}
	void OnTriggerExit(Collider other){
		if (other.tag == "Poin") {
			checkMove = true;
		}
	}
}
