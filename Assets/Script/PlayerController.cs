﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour {
	// Parameter obj
	public float speed;
	public float huong=0;
	public float color;
	// GameObj used in game
	public Slider sld; 
	public GameController gctr;

	// Effect No
	public GameObject PlayerNo;
	public GameObject BulletNo;
	public GameObject BombNo;
	void Awake(){
		gctr = GameObject.Find ("GameControll").GetComponent<GameController> ();
		sld = GameObject.Find ("Slider").GetComponent<Slider> ();
		color = Random.Range (0, 5);

	}
	// Use this for initialization
	void Start () {
		GetComponent<Renderer> ().material = gctr.color [(int)color];
		transform.GetChild(0).GetComponent<Renderer>().material= gctr.color [(int)color];
		transform.GetChild(1).GetComponent<Renderer>().material= gctr.color [(int)color];
		transform.GetChild(2).GetComponent<Renderer>().material= gctr.color [(int)color];
	}
	// Update every seccon
	void Update(){
		//get value slider to set move
		if (sld.value < -0.2f) {
			huong = -1f;
		} else if (sld.value > 0.2f) {
			huong = 1f;
		} else {
			huong = 0;
		}
	}
	// Update is called once per frame
	void FixedUpdate () {
		// move obj down
		this.transform.Translate (Vector3.down * Time.deltaTime * speed * gctr.level);	
		//get vector of obj after set move
		Vector3 vt3=new Vector3 (this.transform.position.x+huong*0.03f,this.transform.position.y,this.transform.position.z);
		// move obj 
		this.transform.position = vt3;
		//check position game obj
		if (this.transform.position.x < -4.2f ) {
			this.transform.position = new Vector3 (-4.2f, this.transform.position.y, this.transform.position.z);
		} else if( this.transform.position.x > 4.2f){
			this.transform.position = new Vector3 (4.2f, this.transform.position.y, this.transform.position.z);
		}
	}
	// run when have any game obj inside this game obj
	void OnTriggerEnter(Collider orther){
		if(orther.tag=="wall"){
			if(orther.GetComponent<WallController>().color==this.color){
				gctr.Got = gctr.Got + 1;
				gctr.Score=gctr.Score+ 1;
				PlayerPrefs.SetInt ("Score",gctr.Score);
			}
			else{
				Debug.Log ("Khong Trung");
				gctr.HP=gctr.HP-1;
				gctr.THP.text = gctr.HP.ToString ();
				PlayerPrefs.SetInt ("HP",(int)gctr.HP);
			}
			Instantiate(PlayerNo,new Vector3(this.transform.position.x,this.transform.position.y,-1),Quaternion.identity);
			Destroy (this.gameObject);
		}

		else if(orther.tag=="Dan"){
			gctr.HP=gctr.HP-1;
			gctr.THP.text = gctr.HP.ToString ();
			PlayerPrefs.SetInt ("HP",(int)gctr.HP);
			Instantiate(PlayerNo,new Vector3(this.transform.position.x,this.transform.position.y,-1),Quaternion.identity);
			Instantiate(BulletNo,new Vector3(this.transform.position.x,this.transform.position.y,-1),Quaternion.identity);
			Destroy (this.gameObject);
			Destroy (orther.gameObject);
		}
		else if(orther.tag=="Bomb"){
			gctr.HP=gctr.HP-1;
			gctr.THP.text = gctr.HP.ToString ();
			PlayerPrefs.SetInt ("HP",(int)gctr.HP);
			Instantiate(PlayerNo,new Vector3(this.transform.position.x,this.transform.position.y,-1),Quaternion.identity);
			Instantiate(BombNo,new Vector3(this.transform.position.x,this.transform.position.y,-1),Quaternion.identity);
			Destroy (this.gameObject);
			Destroy (orther.gameObject);
		}
		else if (orther.tag=="Coin"){
			gctr.Coin = gctr.Coin + 1;
			PlayerPrefs.SetInt ("Coin",(int)gctr.Coin);
			gctr.TCoin.text = gctr.Coin.ToString();
			Destroy (orther.gameObject);
		}
		else if(orther.tag=="HP"){
			gctr.HP = gctr.HP + 1;
			PlayerPrefs.SetInt ("HP",(int)gctr.HP);
			gctr.THP.text = gctr.HP.ToString();
			Destroy (orther.gameObject);
		}
	}
}
