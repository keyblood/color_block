﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SliderController : MonoBehaviour,IPointerUpHandler {
	public void OnPointerUp(PointerEventData data){
		this.transform.GetComponent<Slider>().value = 0;
	}
}
