﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGamescript : MonoBehaviour {
	public GameObject Start;
	public GameObject Exit;
	public GameObject HighScore;
	public GameObject NewGame;
	public GameObject Continute;
	public void StartGame(){

		HideOutButton (true);
	}

	public void ExitGame(){
		Application.Quit ();

	}

	public void HighScoreGame(){
		Application.LoadLevel ("SceneHighScore");

	}

	public void NewGameGame(){	
		PlayerPrefs.SetInt ("Level",1);
		PlayerPrefs.SetInt ("Coin",0);
		PlayerPrefs.SetInt ("Score",0);
		PlayerPrefs.SetInt ("HP",1);
		Application.LoadLevel ("ScenePlay");

	}

	public void ContinuteGame(){
		Application.LoadLevel ("ScenePlay");
	}

	public void HideOutButton(bool check){
		Continute.SetActive (check);
		NewGame.SetActive (check);
		HighScore.SetActive (!check);
		Start.SetActive (!check);
		Exit.SetActive (!check);
	}

}
