﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapController : MonoBehaviour {
	public GameObject Player;

	void FixedUpdate () {
		if (this.transform.childCount < 1) {
			GameObject gob= Instantiate (Player,this.transform.position,Quaternion.identity) as GameObject;
			gob.transform.SetParent (this.transform);
		}
	}
}
