﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour {
	public float level=1f;
	public Text THP;
	public Text TCoin;
	public Text TScore;
	public Text TLevel;
	public Text YouGot;
	public int Score=0;
	public GameObject Pannel;
	public Button Remuse;
	public Button Shop;
	public Button MainMenu;
	public Button Buy;
	public Button RemuseShop;
	public GameObject IHP;
	public float HP=1f;
	public float Coin=0;
	public Material[] color;
	public float[] check;
	public GameObject [] wall;
	public int Got=0;


	void Awake(){
		
		HP = PlayerPrefs.GetInt ("HP",1);
		if (HP < 1) {
			PlayerPrefs.SetInt ("HP",1);
			HP = 1;
		}
		Coin = PlayerPrefs.GetInt ("Coin",0);
		level = PlayerPrefs.GetInt ("Level",1);
		Score = PlayerPrefs.GetInt ("Score",0);
		randomColor ();
		randomColor ();
		randomColor ();
		randomColor ();
	}
	public void Start(){
		addColor ();
		setDefault ();
	}
	void FixedUpdate(){
		YouGot.text = "You Got " + Got + " / " + (4 + level * 2);
	}
	void Update(){
		if (Got > (4 + level * 2)) {
			level += 1;
			PlayerPrefs.SetInt ("Level",(int)level);
			Got = 0;
		}
		if (HP < 1) {
			Application.LoadLevel ("SceneLose");
		}
	}
	void setDefault(){
		TCoin.text = Coin.ToString();
		THP.text = HP.ToString ();
		TScore.text = Score.ToString ();
		TLevel.text = level.ToString ();
	}
	public void Option(){
		Pannel.SetActive (true);
		Time.timeScale = 0f;
	}
	public void ShopButton(){
		Remuse.transform.gameObject.SetActive (false);
		Shop.transform.gameObject.SetActive(false);
		MainMenu.transform.gameObject.SetActive(false);
		Buy.transform.gameObject.SetActive (true);
		RemuseShop.transform.gameObject.SetActive (true);
		IHP.transform.gameObject.SetActive (true);
	}
	public void RemuseButton(){
		Pannel.SetActive (false);
		Remuse.transform.gameObject.SetActive (true);
		Shop.transform.gameObject.SetActive(true);
		MainMenu.transform.gameObject.SetActive(true);
		Buy.transform.gameObject.SetActive (false);
		RemuseShop.transform.gameObject.SetActive (false);
		IHP.transform.gameObject.SetActive (false);
		Time.timeScale = 1f;
	}
	public void MainMenuButton(){
		Time.timeScale = 1;
		Application.LoadLevel ("SceneMain");
	}
	public void BuyButton(){
		if (Coin >= 1) {
			HP = HP + 1;
			Coin = Coin - 1;
			PlayerPrefs.SetInt ("HP",(int)HP);
			PlayerPrefs.SetInt ("Coin",(int)Coin);
			THP.text = HP.ToString();
			TCoin.text = Coin.ToString();
		}
	}
	public void randomColor(){
		int i=Random.Range(0,5);
		int f=Random.Range(0,5);
		float temp=check[i];
		check [i] = check [f];
		check [f] = temp;
	}
	public void addColor(){
		for (int i = 0; i < 5; i++) {
			wall[i].GetComponent<WallController>().color=check[i];
			wall [i].GetComponent<Renderer> ().material = color [(int)check[i]];
			wall[i].transform.GetChild(0).GetComponent<Renderer>().material=color [(int)check[i]];
			wall[i].transform.GetChild(1).GetComponent<Renderer>().material=color [(int)check[i]];
			wall[i].transform.GetChild(2).GetComponent<Renderer>().material=color [(int)check[i]];
		}
	}
}
