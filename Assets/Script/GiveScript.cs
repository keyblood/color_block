﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveScript : MonoBehaviour {
	public GameObject[] Item;
	float time;
	float timeDelay=3f;
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (time > timeDelay &&this.transform.childCount<2) {
		GameObject obj=	Instantiate (Item[Random.Range(0,Item.Length)],new Vector3(this.transform.position.x+Random.Range(-4,4f),this.transform.position.y,this.transform.position.z),Quaternion.identity) as GameObject;
			obj.transform.SetParent(this.transform);
		}
	}
}
